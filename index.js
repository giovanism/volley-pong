var isNewGame = true;
var paused = false;
var pauseLock = false;

var gl;
var shaderProgram;
var resolution;
var mvMatrix;
var drawingMVMatrices = [];
var drawingVertexBuffers = [];
var drawingColorBuffers = [];
var currentlyPressedKeys = {};

var player1;
var player2;
var ball;
var net;

var blueScore = 0;
var redScore = 0;

var blueScoreView;
var redScoreView;

var pauseView;

// Show "Cara Bermain" section
function showHowTo() {
    document.getElementById("menu-2").style.display = "flex";
    document.getElementById("menu-1").style.display = "none";
}

// Hide "Cara Bermain" section
function hideHowTo() {
    document.getElementById("menu-1").style.display = "flex";
    document.getElementById("menu-2").style.display = "none";
}

// Pause the game
function switchPause() {
    if (!pauseLock) {
        paused = !paused;
        pauseLock = true;
        pauseView.innerHTML = `<div>${(isNewGame ? `<p>Start Game?</p>` : `<p>Game Paused</p>`)}<h6>Press Space</h6></div>`;
        isNewGame = false;

        if(paused) {
            pause.classList.add("active");
            document.getElementById("pauseInfo").style.display = "none";
        } else {
            pause.classList.remove("active");
            document.getElementById("pauseInfo").style.display = "flex";
        }

        setTimeout(() => {
            pauseLock = false;
        }, 500);
    }
}

// Make Player object
function Player(locStart, color) {
    this.location = Object.create(locStart);
    this.width = 80;
    this.height = 80;

    this.velocity = 8.0;

    this.mvMatrix = mat4.create();
    mat4.identity(this.mvMatrix);
    mat4.translate(this.mvMatrix, normalToClip(Object.create(locStart)));

    this.vertices = [
        -40.0, 40.0,
        40.0, 40.0,
        -40.0, -40.0,
        40.0, -40.0
    ];
    this.color = color;
    initBuffers(this);
}

// Make Ball object
function Ball(locStart, color, velStart, accelStart) {
    this.location = Object.create(locStart);
    this.width = 20;
    this.height = 20;

    this.acceleration = accelStart;
    this.velocity = velStart;

    this.mvMatrix = mat4.create();
    mat4.identity(this.mvMatrix);
    mat4.translate(this.mvMatrix, normalToClip(Object.create(locStart)));

    this.vertices = [
        -10.0, 10.0,
        10.0, 10.0,
        -10.0, -10.0,
        10.0, -10.0
    ];
    this.color = color;
    initBuffers(this);
}

// Make Net object
function Net(locStart, color) {
    this.location = Object.create(locStart);
    this.width = 16;
    this.height = 250;

    this.velocity = [0.0, 0.0, 0.0]
    this.mvMatrix = mat4.create();
    mat4.identity(this.mvMatrix);
    mat4.translate(this.mvMatrix, normalToClip(Object.create(locStart)));

    this.vertices = [
        -8.0, 125.0,
        8.0, 125.0,
        -8.0, -125.0,
        8.0, -125.0,
    ]
    this.color = color;
    initBuffers(this);
}

function initGL(canvas) {
    try {
        gl = canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
        gl.viewportWidth = canvas.width;
        gl.viewportHeight = canvas.height;
        resolution = [canvas.width, canvas.height, 1.0];
    } catch (e) {
    }
    if (!gl) {
        alert("Could not initialize WebGL, sorry :-(");
    }
}

function getShader(gl, id) {
    var shaderScript = document.getElementById(id);
    if (!shaderScript) {
        return null;
    }

    var str = "";
    var k = shaderScript.firstChild;
    while (k) {
        if (k.nodeType == 3) {
            str += k.textContent;
        }
        k = k.nextSibling;
    }

    var shader;
    if (shaderScript.type == "x-shader/x-fragment") {
        shader = gl.createShader(gl.FRAGMENT_SHADER);
    } else if (shaderScript.type == "x-shader/x-vertex") {
        shader = gl.createShader(gl.VERTEX_SHADER);
    } else {
        return null;
    }

    gl.shaderSource(shader, str);
    gl.compileShader(shader);

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        alert(gl.getShaderInfoLog(shader));
        return null;
    }
    return shader;
}

function initShaders() {
    var fragmentShader = getShader(gl, "shader-fs");
    var vertexShader = getShader(gl, "shader-vs");

    shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);
    gl.linkProgram(shaderProgram);

    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
        alert("Could not initialize shaders");
    }

    gl.useProgram(shaderProgram);
    shaderProgram.resolutionUniform = gl.getUniformLocation(shaderProgram, "uResolution")	// Resolution

    shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
    gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);

    shaderProgram.vertexColorAttribute = gl.getAttribLocation(shaderProgram, "aVertexColor");
    gl.enableVertexAttribArray(shaderProgram.vertexColorAttribute);

    shaderProgram.mvMatrixUniform = gl.getUniformLocation(shaderProgram, "uMVMatrix");
}

function setMatrixUniforms() {
    gl.uniform2f(shaderProgram.resolutionUniform, canvas.width, canvas.height);	// Resolution
    gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, mvMatrix);
}

function initObjects(direction = -1) {
    player1 = new Player([250.0, 500.0, 0.0], [0.0, 0.0, 1.0, 1.0]);
    player2 = new Player([750.0, 500.0, 0.0], [1.0, 0.0, 0.0, 1.0]);
    net = new Net([canvas.width / 2.0, 375.0, 0.0], [0.0, 0.0, 0.0, 1.0]);
    ball = new Ball([canvas.width / 2.0, canvas.height / 2.0 - 40, 0.0],
                    [0.0, 1.0, 0.0, 1.0],
                    [direction * 3.5, 0.0, 0.0],
                    [0.0, -0.1, 0.0]);
}

function initBuffers(object) {
    object.vertexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, object.vertexBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(object.vertices), gl.STATIC_DRAW);
    object.vertexBuffer.itemSize = 2;
    object.vertexBuffer.numItems = 4;

    object.colors = [];
    for (var i = 0; i < 4; i++) {
        object.colors = object.colors.concat(object.color);
    }
    object.colorBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, object.colorBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(object.colors), gl.STATIC_DRAW);
    object.colorBuffer.itemSize = 4;
    object.colorBuffer.numItems = 4;

    drawingMVMatrices.push(object.mvMatrix);
    drawingVertexBuffers.push(object.vertexBuffer);
    drawingColorBuffers.push(object.colorBuffer);
}

function refillBuffers(object) {
    drawingMVMatrices.push(object.mvMatrix);
    drawingVertexBuffers.push(object.vertexBuffer);
    drawingColorBuffers.push(object.colorBuffer);
}

function handleKeyDown(event) {
    currentlyPressedKeys[event.keyCode] = true;
}

function handleKeyUp(event) {
    currentlyPressedKeys[event.keyCode] = false;
}

function handleKeys() {
    if (currentlyPressedKeys[32]) {
        // Space key
        switchPause();
    }

    if (paused) {
        return;
    }

    if (currentlyPressedKeys[87]) {
        // W key
        mat4.translate(player1.mvMatrix, normalToClip([0.0, -player1.velocity, 0.0]));
        player1.location = vec3.add(player1.location, [0.0, -player1.velocity, 0.0]);
    }
    if (currentlyPressedKeys[65]) {
        // A key
        mat4.translate(player1.mvMatrix, normalToClip([-player1.velocity, 0.0, 0.0]));
        player1.location = vec3.add(player1.location, [-player1.velocity, 0.0, 0.0]);
    }
    if (currentlyPressedKeys[83]) {
        // S keya
        mat4.translate(player1.mvMatrix, normalToClip([0.0, player1.velocity, 0.0]));
        player1.location = vec3.add(player1.location, [0.0, player1.velocity, 0.0]);
    }
    if (currentlyPressedKeys[68]) {
        // D key
        mat4.translate(player1.mvMatrix, normalToClip([player1.velocity, 0.0, 0.0]));
        player1.location = vec3.add(player1.location, [player1.velocity, 0.0, 0.0]);
    }
    if (currentlyPressedKeys[37]) {
        // Left cursor key
        mat4.translate(player2.mvMatrix, normalToClip([-player2.velocity, 0.0, 0.0]));
        player2.location = vec3.add(player2.location, [-player2.velocity, 0.0, 0.0]);
    }
    if (currentlyPressedKeys[38]) {
        // Up cursor key
        mat4.translate(player2.mvMatrix, normalToClip([0.0, -player2.velocity, 0.0]));
        player2.location = vec3.add(player2.location, [0.0, -player2.velocity, 0.0]);
    }
    if (currentlyPressedKeys[39]) {
        // Right cursor key
        mat4.translate(player2.mvMatrix, normalToClip([player2.velocity, 0.0, 0.0]));
        player2.location = vec3.add(player2.location, [player2.velocity, 0.0, 0.0]);
    }
    if (currentlyPressedKeys[40]) {
        // Down cursor key
        mat4.translate(player2.mvMatrix, normalToClip([0.0, player2.velocity, 0.0]));
        player2.location = vec3.add(player2.location, [0.0, player2.velocity, 0.0]);
    }
}

function checkCollision() {
    // Player1 - Collision vertical boundary
    if (player1.location[1] - (player1.height / 2.0) <= canvas.height / 2.0) {
        mat4.translate(player1.mvMatrix, normalToClip([0, canvas.height / 2.0 + (player1.height / 2.0) - player1.location[1], 0]));
        player1.location[1] = canvas.height / 2.0 + (player1.height / 2.0);
    }

    if (player1.location[1] + (player1.height / 2.0) >= canvas.height) {
        mat4.translate(player1.mvMatrix, normalToClip([0, canvas.height - (player1.location[1] + (player1.height / 2.0)), 0]));
        player1.location[1] = canvas.height - (player1.height / 2.0);
    }

    // Player 1 - Collision horizontal boundary
    if (player1.location[0] - (player1.width / 2.0) <= 0) {
        mat4.translate(player1.mvMatrix, normalToClip([(player1.width / 2.0) - player1.location[0], 0, 0]));
        player1.location[0] = (player1.width / 2.0);
    }

    // Player2 - Collision vertical boundary
    if (player2.location[1] - (player2.height / 2.0) <= canvas.height / 2.0) {
        mat4.translate(player2.mvMatrix, normalToClip([0, canvas.height / 2.0 + (player2.height / 2.0) - player2.location[1], 0]));
        player2.location[1] = canvas.height / 2.0 + (player2.height / 2.0);
    }

    if (player2.location[1] + (player2.height / 2.0) >= canvas.height) {
        mat4.translate(player2.mvMatrix, normalToClip([0, canvas.height - (player2.location[1] + (player2.height / 2.0)), 0]));
        player2.location[1] = canvas.height - (player2.height / 2.0);
    }

    // Player 2 - sCollision horizontal boundary
    if (player2.location[0] + (player2.width / 2.0) >= canvas.width) {
        mat4.translate(player2.mvMatrix, normalToClip([canvas.width - (player2.location[0] + (player2.width / 2.0)), 0, 0]));
        player2.location[0] = canvas.width - (player2.width / 2.0);
    }

    // Player 1 - Net collisions
    if (player1.location[0] + (player1.width / 2.0) >= net.location[0] - net.width / 2.0) {
        mat4.translate(player1.mvMatrix, normalToClip([net.location[0] - net.width / 2.0 - player1.width / 2.0 - player1.location[0], 0, 0]));
        player1.location[0] = net.location[0] - net.width / 2.0 - (player1.width / 2.0);
    }

    // Player 2 - Net collisions
    if (player2.location[0] - (player2.width / 2.0) <= net.location[0] + net.width / 2.0) {
        mat4.translate(player2.mvMatrix, normalToClip([net.location[0] + net.width / 2.0 + (player2.width / 2.0) - player2.location[0], 0, 0]));
        player2.location[0] = net.location[0] + net.width / 2.0 + (player2.width / 2.0);
    }

    // Player1 - Right side hits ball
    if (Math.abs(ball.location[0] - player1.location[0]) <= (ball.width / 2.0) + (player1.width / 2.0) &&
        ball.location[0] - player1.location[0] >= 0 &&
        Math.abs(player1.location[1] - ball.location[1]) <= Math.abs(player1.location[0] - ball.location[0]) &&
        Math.abs(ball.location[1] - player1.location[1]) <= (ball.height / 2.0) + (player1.height / 2.0)) {
        vec3.multiply(ball.velocity, [-1.0, 1.0, 1.0]);
        vec3.multiply(ball.velocity, [1.1, 1.1, 1.0]);
        mat4.translate(ball.mvMatrix, normalToClip([(((ball.width / 2.0) + (player1.width / 2.0)) - Math.abs(ball.location[0] - player1.location[0])), 0, 0]));
        ball.location[0] = player1.location[0] + (ball.width / 2.0) + (player1.width / 2.0);
    }

    // Player1 - Left side hits ball
    if (Math.abs(ball.location[0] - player1.location[0]) <= (ball.width / 2.0) + (player1.width / 2.0) &&
        ball.location[0] - player1.location[0] <= 0 &&
        Math.abs(player1.location[1] - ball.location[1]) <= Math.abs(player1.location[0] - ball.location[0]) &&
        Math.abs(ball.location[1] - player1.location[1]) <= (ball.height / 2.0) + (player1.height / 2.0)) {
        vec3.multiply(ball.velocity, [-1.0, 1.0, 1.0]);
        vec3.multiply(ball.velocity, [1.1, 1.1, 1.0]);
        mat4.translate(ball.mvMatrix, normalToClip([-(((ball.width / 2.0) + (player1.width / 2.0)) - Math.abs(ball.location[0] - player1.location[0])), 0, 0]));
        ball.location[0] = player1.location[0] - (ball.width / 2.0) - (player1.width / 2.0);
    }

    // Player2 - Right side hits ball
    if (Math.abs(ball.location[0] - player2.location[0]) <= (ball.width / 2.0) + (player2.width / 2.0) &&
        ball.location[0] - player2.location[0] <= 0 &&
        Math.abs(player2.location[1] - ball.location[1]) <= Math.abs(player2.location[0] - ball.location[0]) &&
        Math.abs(ball.location[1] - player2.location[1]) <= (ball.height / 2.0) + (player2.height / 2.0)) {
        vec3.multiply(ball.velocity, [-1.0, 1.0, 1.0]);
        vec3.multiply(ball.velocity, [1.1, 1.1, 1.0]);
        mat4.translate(ball.mvMatrix, normalToClip([-(((ball.width / 2.0) + (player2.width / 2.0)) - Math.abs(ball.location[0] - player2.location[0])), 0, 0]));
        ball.location[0] = player2.location[0] - (ball.width / 2.0) - (player2.width / 2.0);
    }

    // Player2 - Right side hits ball
    if (Math.abs(ball.location[0] - player2.location[0]) <= (ball.width / 2.0) + (player2.width / 2.0) &&
        ball.location[0] - player2.location[0] >= 0 &&
        Math.abs(player2.location[1] - ball.location[1]) <= Math.abs(player2.location[0] - ball.location[0]) &&
        Math.abs(ball.location[1] - player2.location[1]) <= (ball.height / 2.0) + (player2.height / 2.0)) {
        vec3.multiply(ball.velocity, [-1.0, 1.0, 1.0]);
        vec3.multiply(ball.velocity, [1.1, 1.1, 1.0]);
        mat4.translate(ball.mvMatrix, normalToClip([(((ball.width / 2.0) + (player2.width / 2.0)) - Math.abs(ball.location[0] - player2.location[0])), 0, 0]));
        ball.location[0] = player2.location[0] + (ball.width / 2.0) + (player2.width / 2.0);
    }

    // Player1 - Top side hits ball
    if (Math.abs(ball.location[0] - player1.location[0]) <= (ball.width / 2.0) + (player1.width / 2.0) &&
        ball.location[1] - player1.location[1] <= 0 &&
        Math.abs(player1.location[1] - ball.location[1]) > Math.abs(player1.location[0] - ball.location[0]) &&
        Math.abs(ball.location[1] - player1.location[1]) <= (ball.height / 2.0) + (player1.height / 2.0)) {
        let bias = 8.0 * (ball.location[0] - player1.location[0]) / player1.width;
        ball.velocity[0] += bias;
        vec3.multiply(ball.velocity, [1.0, -1.0, 1.0]);
        vec3.multiply(ball.velocity, [1.1, 1.1, 1.0]);
        mat4.translate(ball.mvMatrix, normalToClip([0, -(((ball.height / 2.0) + (player1.height / 2.0)) - Math.abs(ball.location[1] - player1.location[1])), 0]));
        ball.location[1] = player1.location[1] - (ball.height / 2.0) - (player1.height / 2.0);
    }

    // Player2 - Top side hits ball
    if (Math.abs(ball.location[0] - player2.location[0]) <= (ball.width / 2.0) + (player2.width / 2.0) &&
        ball.location[1] - player2.location[1] <= 0 &&
        Math.abs(player2.location[1] - ball.location[1]) > Math.abs(player2.location[0] - ball.location[0]) &&
        Math.abs(ball.location[1] - player2.location[1]) <= (ball.height / 2.0) + (player2.height / 2.0)) {
        let bias = 8.0 * (ball.location[0] - player2.location[0]) / player2.width;
        ball.velocity[0] += bias;
        vec3.multiply(ball.velocity, [1.0, -1.0, 1.0]);
        vec3.multiply(ball.velocity, [1.1, 1.1, 1.0]);
        mat4.translate(ball.mvMatrix, normalToClip([0, -(((ball.height / 2.0) + (player2.height / 2.0)) - Math.abs(ball.location[1] - player2.location[1])), 0]));
        ball.location[1] = player2.location[1] - (ball.height / 2.0) - (player2.height / 2.0);
    }

    // Ball hits the left side of the net
    if (Math.abs(ball.location[0] - net.location[0]) <= (ball.width / 2.0) + (net.width / 2.0) &&
        ball.location[0] - net.location[0] <= 0 &&
        Math.abs(net.location[1] - ball.location[1]) - (net.height / 2.0) <= Math.abs(net.location[0] - ball.location[0]) - (net.width / 2.0)  &&
        Math.abs(ball.location[1] - net.location[1]) <= (ball.height / 2.0) + (net.height / 2.0)) {
        vec3.multiply(ball.velocity, [-1.0, 1.0, 1.0]);
        vec3.multiply(ball.velocity, [1.0, 1.0, 1.0]);
        mat4.translate(ball.mvMatrix, normalToClip([-(((ball.width / 2.0) + (net.width / 2.0)) - Math.abs(ball.location[0] - net.location[0])), 0, 0]));
        ball.location[0] = net.location[0] - (ball.width / 2.0) - (net.width / 2.0);
    }

    // Ball hits the right side of the net
    if (Math.abs(ball.location[0] - net.location[0]) <= (ball.width / 2.0) + (net.width / 2.0) &&
        ball.location[0] - net.location[0] >= 0 &&
        Math.abs(net.location[1] - ball.location[1]) - (net.height / 2.0) <= Math.abs(net.location[0] - ball.location[0]) - (net.width / 2.0)  &&
        Math.abs(ball.location[1] - net.location[1]) <= (ball.height / 2.0) + (net.height / 2.0)) {
        vec3.multiply(ball.velocity, [-1.0, 1.0, 1.0]);
        vec3.multiply(ball.velocity, [1.0, 1.0, 1.0]);
        mat4.translate(ball.mvMatrix, normalToClip([(((ball.width / 2.0) + (net.width / 2.0)) - Math.abs(ball.location[0] - net.location[0])), 0, 0]));
        ball.location[0] = net.location[0] + (ball.width / 2.0) + (net.width / 2.0);
    }

    // Net - Top hits ball
    if (Math.abs(ball.location[0] - net.location[0]) <= (ball.width / 2.0) + (net.width / 2.0) &&
        ball.location[1] - net.location[1] <= 0 &&
        Math.abs(net.location[1] - ball.location[1]) - (net.height / 2.0) > Math.abs(net.location[0] - ball.location[0]) - (net.width / 2.0)  &&
        Math.abs(ball.location[1] - net.location[1]) <= (ball.height / 2.0) + (net.height / 2.0)) {
        vec3.multiply(ball.velocity, [1.0, -1.0, 1.0]);
        vec3.multiply(ball.velocity, [1.0, 1.0, 1.0]);
        mat4.translate(ball.mvMatrix, normalToClip([0, -(((ball.height / 2.0) + (net.height / 2.0)) - Math.abs(ball.location[1] - net.location[1])), 0]));
        ball.location[1] = net.location[1] - (ball.height / 2.0) - (net.height / 2.0);
    }

    // Ball vertical boundary
    if ((ball.location[1] - (ball.height / 2.0)) < 0.0) {
        vec3.multiply(ball.velocity, [1.0, -1.0, 1.0]);
        vec3.multiply(ball.velocity, [1.0, 1.0, 1.0]);
    }

    // Ball floor collision and game score/restart logic
    if ((ball.location[1] + (ball.height / 2.0)) >= canvas.height) {
        let direction;
        if(ball.location[0] < net.location[0]) {
            direction = 1;
            redScore++;
            redScoreView.innerHTML = redScore;
        } else {
            direction = -1;
            blueScore++;
            blueScoreView.innerHTML = blueScore;
        }
        initObjects(direction);
        isNewGame = true;
        setTimeout(() => switchPause(), 20);
    }

    // Ball horizontal boundary
    if ((ball.location[0] - (ball.width / 2.0)) < 0.0 || (ball.location[0] + (ball.width / 2.0)) >= canvas.width) {
        vec3.multiply(ball.velocity, [-1.0, 1.0, 1.0]);
        vec3.multiply(ball.velocity, [1.0, 1.0, 1.0]);
    }
}

function drawScene() {
    gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    var currentMVMatrix;
    var vertexBuffer;
    var colorBuffer;
    for (var i = drawingVertexBuffers.length; i > 0; i--) {
        mvMatrix = drawingMVMatrices.pop();
        vertexBuffer = drawingVertexBuffers.pop();
        colorBuffer = drawingColorBuffers.pop();
        gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
        gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, vertexBuffer.itemSize, gl.FLOAT, false, 0, 0);
        gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
        gl.vertexAttribPointer(shaderProgram.vertexColorAttribute, colorBuffer.itemSize, gl.FLOAT, false, 0, 0);
        setMatrixUniforms();
        gl.drawArrays(gl.TRIANGLE_STRIP, 0, vertexBuffer.numItems);
    }
}

function normalToClip(src) {
    var zeroToOne = vec3.divide(src, resolution);
    var zeroToTwo = vec3.multiply(zeroToOne, [2.0, 2.0, 2.0]);
    var dest = vec3.multiply(zeroToTwo, [1.0, -1.0, 0.0]);
    return dest;
}

function tick() {
    requestAnimFrame(tick);
    handleKeys();
    if (!paused) {
        checkCollision();
        drawScene();
        animate();
    }
}

var lastTime = 0;
function animate() {
    var timeNow = new Date().getTime();
    if (lastTime != 0) {
        var elapsed = timeNow - lastTime;

        // Apply ball acceleration
        ball.velocity[0] = ball.velocity[0] - ball.acceleration[0];
        ball.velocity[1] = ball.velocity[1] - ball.acceleration[1];
        ball.velocity[2] = ball.velocity[2] - ball.acceleration[2];

        mat4.translate(ball.mvMatrix, normalToClip(Object.create(ball.velocity)));
        ball.location = vec3.add(ball.location, ball.velocity);
    }
    lastTime = timeNow;

    refillBuffers(player1);
    refillBuffers(player2);
    refillBuffers(ball);
    refillBuffers(net);
}

function webGLStart() {
    var canvas = document.getElementById("canvas");
    initGL(canvas);
    initShaders();
    initObjects();

    gl.clearColor(1.0, 1.0, 1.0, 1.0);
    gl.enable(gl.DEPTH_TEST);

    document.onkeydown = handleKeyDown;
    document.onkeyup = handleKeyUp;

    blueScoreView = document.getElementById("blue-score");
    redScoreView = document.getElementById("red-score");

    blueScoreView.innerHTML = blueScore;
    redScoreView.innerHTML = redScore;

    pauseView = document.getElementById("pause");

    tick();
    setTimeout(() => switchPause(), 20);
}

var spaceCode = 32;

// Player2
var arrowUpCode = 38;
var arrowLeftCode = 37;
var arrowRightCode = 39;

// Player1
var WCode = 87;
var DCode = 68;

async function startDemo() {
    document.onkeydown = false;
    blueScore = 0;
    blueScoreView.innerHTML = 0;
    redScore = 0;
    redScoreView.innerHTML = 0;

    currentlyPressedKeys[spaceCode] = true;
    setTimeout(() => {currentlyPressedKeys[spaceCode] = false}, 20)
    demoPlayer1();
    demoPlayer2();
    document.onkeydown = false;
}

async function demoPlayer1() {
    await createPromise(() => {}, 1200);
    await createPromise(() => {currentlyPressedKeys[WCode] = true}, 600);
    currentlyPressedKeys[WCode] = false;
    await createPromise(() => {currentlyPressedKeys[DCode] = true}, 400);
    currentlyPressedKeys[DCode] = false;
}

async function demoPlayer2() {
    await createPromise(() => {}, 1900);
    await createPromise(() => {currentlyPressedKeys[arrowRightCode] = true}, 800);
    currentlyPressedKeys[arrowRightCode] = false;
    await createPromise(() => {currentlyPressedKeys[arrowUpCode] = true}, 400);
    currentlyPressedKeys[arrowUpCode] = false;
    await createPromise(() => {currentlyPressedKeys[arrowLeftCode] = true}, 200);
    currentlyPressedKeys[arrowLeftCode] = false;
}

var createPromise = (func, timeout) => {
    return new Promise(resolve => {
        func();
        setTimeout(() => {
            resolve("Success!")
        }, timeout);
    })
}