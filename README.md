# VolleyPong WebGL

_Repository_ ini berisi kode sumber untuk game VolleyPong yang dibuat untuk memenuhi pengerjaan **nomor satu** Ujian Tengah Semester mata kuliah Grafika Komputer Semester Genap 2019/2020, Fakultas Ilmu Komputer, Universitas Indonesia.

![VolleyPong Homepage](https://files.catbox.moe/1qa0zn.png)
![VolleyPong Plays](https://files.catbox.moe/7810wc.png)

## Pembagian Tugas Pengembang

Gagah Pangeran Rosfatiputra (1706039566)
- Mengimplementasikan gerakan pemain.
- Mengimplementasikan kondisi _collision_ antara pemain dan bola.
- Mengimplementasikan kondisi _collision_ antara bola dan net.
- Mengimplementasikan kondisi _collision_ antara pemain dan tembok serta net.
- Membuat pop-up start dan pause.
- Membuat asynchronous Promise untuk fungsi demo.
- Membuat fungsi score.

Giovan Isa Musthofa (1706040126)
- Membuat Net.
- Membuat mapping ke keyCode keyboard.
- Mengimplementasikan gravitasi bola.
- Mengimplementasikan _collision_ sisi atas/_top_ pemain dan net dengan bola.
- Membuat sistem pause.
- Menulis sistem "Mainkan Demo".

Roshani Ayu Pranasti (1706026052)
- Meng-_init_ canvas.
- Membuat Player1, Player2 dan Ball.
- Membuat fungsi showHowTo dan hideHowTo.
- Membuat seluruh tampilan HTML dan CSS.
- Membuat demo video.
- Membuat readme.md dan pdf.

## Deskripsi Game

VolleyPong adalah sebuah permainan bola voli yang dikembangkan dengan HTML, CSS, JavaScript, dan WebGL. Permainan ini memiliki gameplay dinamis dan kontrol yang ramah di mana terdapat **dua pemain** berbentuk persegi melawan satu sama lain untuk memasukkan bola hijau ke area lawan dan mempertahankan bola tersebut agar tidak jatuh di area masing-masing pemain.

## Cara Bermain

1. Memulai permainan dengan menekan key **Space**.
2. Menggerakkan pemain sesuai warna nya.
   - Blue player: key **W** (atas), key **A** (kiri), key **S** (bawah), key **D** (kanan)\
     <img src="https://files.catbox.moe/l685l1.png"  width="400">
   - Red player: key **Up** (atas), key **Left** (kiri), key **Down** (bawah), key **Right** (kanan)\
     <img src="https://files.catbox.moe/xzgz3g.png"  width="400">
3. Menjatuhkan bola hijau ke area lawan. Jangan biarkan bola hijau jatuh di area kamu.
4. Selamat bermain!
